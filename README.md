[![Build Status](https://gitlab.com/takatan/rsdfind/badges/master/pipeline.svg)](https://gitlab.com/takatan/rsdfind/commits/master)
![Maintenance](https://img.shields.io/badge/maintenance-activly--developed-brightgreen.svg)

# rsdfind

Duplicate file find tool.

Usage: rsdfind [options] FILE or DIR ...

License: GPL-3.0-or-later
