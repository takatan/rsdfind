use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use std::os::unix::fs::MetadataExt;
use std::path::{Path, PathBuf};

use itertools::Itertools;
use itertools::__std_iter::Sum;
use walkdir::WalkDir;

use crate::filter_condition::FilterCondition;
use crate::grouping_condition::GroupingCondition;
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct FileGroup {
    pub files: Vec<PathBuf>,
}

impl FileGroup {
    pub fn new(path: &str) -> Self {
        Self {
            files: WalkDir::new(path)
                .into_iter()
                .map(|e| e.unwrap().into_path())
                .filter(|p| p.is_file())
                .collect::<Vec<_>>(),
        }
    }

    pub fn group_by<T: Eq + Ord + Debug>(
        &self,
        condition: &GroupingCondition<T>,
    ) -> Vec<FileGroup> {
        self.files
            .iter()
            .sorted_by_key(|p| (condition.predicate)(p))
            // .inspect(|p| println!("inspect {} {:?} key={:?}", condition.title, p, (condition.predicate)(p)))
            .group_by(|p| (condition.predicate)(p))
            .into_iter()
            .map(|(_, group)| FileGroup {
                files: group.cloned().collect::<Vec<_>>(),
            })
            .filter(|g| g.len() > 1)
            .collect::<Vec<_>>()
    }

    pub fn filter_by(&self, condition: &FilterCondition) -> Self {
        Self {
            files: self
                .files
                .iter()
                .filter(|p| (condition.predicate)(p))
                .cloned()
                .collect(),
        }
    }

    pub fn filter_inode_duplicate(&self) -> Self {
        let dups = inode_duplicate(&self.files);
        Self {
            files: self
                .files
                .iter()
                .filter(|p| !dups.contains(p))
                .cloned()
                .collect(),
        }
    }
    pub fn len(&self) -> usize {
        self.files.len()
    }

    pub fn file_size(&self) -> u64 {
        self.files.iter().map(|f| f.metadata().unwrap().len()).sum()
    }
}

impl Display for FileGroup {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "count = {}, size = {}",
            self.files.len(),
            self.files[0].metadata().unwrap().len()
        )?;
        for file in self.files.iter() {
            writeln!(f, "{}", file.to_str().unwrap())?;
        }
        Ok(())
    }
}

impl Sum<FileGroup> for FileGroup {
    fn sum<I: Iterator<Item = FileGroup>>(iter: I) -> Self {
        FileGroup {
            files: iter.flat_map(|g| g.files).collect(),
        }
    }
}

fn dev_ino(entry: &Path) -> (u64, u64) {
    // entry.metadata().unwrap()
    (
        entry.metadata().unwrap().dev(),
        entry.metadata().unwrap().ino(),
    )
}

fn inode_duplicate(paths: &[PathBuf]) -> HashSet<&PathBuf> {
    let same_inode_groups: Vec<Vec<&PathBuf>> = paths
        .iter()
        .sorted_by_key(|p| dev_ino(p))
        .group_by(|p| dev_ino(p))
        .into_iter()
        .map(|(_, group)| group.collect::<Vec<_>>())
        .filter(|x| x.len() > 1)
        .collect();
    same_inode_groups
        .iter()
        .flat_map(|group| &group[1..])
        .cloned()
        .collect()
}

#[cfg(test)]
mod test {
    use std::fs::{create_dir, hard_link, File};

    use tempdir::TempDir;

    use crate::file_group::{dev_ino, inode_duplicate, FileGroup};
    use std::collections::HashSet;

    #[test]
    fn test_sum() {
        // let fs = FakeFileSystem::new();
        let tmp = TempDir::new("test").unwrap();

        create_dir(tmp.path().join("a")).unwrap();
        File::create(tmp.path().join("a/0")).unwrap();
        File::create(tmp.path().join("a/1")).unwrap();
        create_dir(tmp.path().join("b")).unwrap();
        File::create(tmp.path().join("b/x")).unwrap();
        File::create(tmp.path().join("b/y")).unwrap();
        File::create(tmp.path().join("b/z")).unwrap();
        let a = FileGroup::new(tmp.path().join("a").to_str().unwrap());
        let b = FileGroup::new(tmp.path().join("b").to_str().unwrap());
        assert_eq!(a.len(), 2);
        assert_eq!(b.len(), 3);
        let s = vec![a, b].into_iter().sum::<FileGroup>();
        assert_eq!(s.len(), 5);
    }

    #[test]
    fn test_inode_duplicate() {
        let tmp = TempDir::new("test").unwrap();
        let origin = tmp.path().join("0");
        let target0 = tmp.path().join("0_link0");
        let target1 = tmp.path().join("0_link1");
        let another = tmp.path().join("1");
        File::create(&origin).unwrap();
        hard_link(&origin, &target0).unwrap();
        hard_link(&origin, &target1).unwrap();
        File::create(&another).unwrap();
        assert_eq!(
            inode_duplicate(&[origin, target0.clone(), target1.clone(), another]),
            [target0, target1].iter().collect::<HashSet<_>>()
        );
    }

    #[test]
    fn test_filter_inode_duplicate() {
        let tmp = TempDir::new("test").unwrap();
        let origin = tmp.path().join("0");
        let target0 = tmp.path().join("0_link0");
        let target1 = tmp.path().join("0_link1");
        let another = tmp.path().join("1");
        File::create(&origin).unwrap();
        hard_link(&origin, target0).unwrap();
        hard_link(&origin, target1).unwrap();
        File::create(&another).unwrap();
        let result = FileGroup::new(tmp.path().to_str().unwrap()).filter_inode_duplicate();
        assert_eq!(
            result
                .files
                .iter()
                .map(|f| dev_ino(f))
                .collect::<HashSet<_>>(),
            [origin, another]
                .iter()
                .map(|f| dev_ino(f))
                .collect::<HashSet<_>>()
        );
    }
}
