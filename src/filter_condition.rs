use std::path::PathBuf;

pub struct FilterCondition {
    pub title: &'static str,
    pub predicate: fn(&PathBuf) -> bool,
}

pub const SIZE_NOT_ZERO: FilterCondition = FilterCondition {
    title: "not empty",
    predicate: |path| path.metadata().unwrap().len() > 0,
};

#[cfg(test)]
mod test {
    use std::fs::File;
    use std::io::Write;
    use tempdir::TempDir;

    use crate::filter_condition::SIZE_NOT_ZERO;

    #[test]
    fn test_size_not_zero() {
        let tmp = TempDir::new("test").unwrap();
        let not_empty = tmp.path().to_path_buf().join("not_empty");
        let empty = tmp.path().to_path_buf().join("empty");
        {
            let mut fp = File::create(tmp.path().join(&not_empty)).unwrap();
            fp.write_all("a".as_bytes()).unwrap();
        }
        File::create(&empty).unwrap();
        assert!((SIZE_NOT_ZERO.predicate)(&not_empty));
        assert!(!(SIZE_NOT_ZERO.predicate)(&empty));
    }
}
