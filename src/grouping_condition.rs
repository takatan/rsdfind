use crypto::digest::Digest;
use crypto::sha1::Sha1;
use std::cmp::min;
use std::fs::{read, File};
use std::io::{Read, Seek, SeekFrom};
use std::path::PathBuf;

const SIZE: usize = 512;

pub struct GroupingCondition<T>
where
    T: Eq + Ord,
{
    pub title: &'static str,
    pub predicate: fn(&PathBuf) -> T,
}

pub const FILE_SIZE: GroupingCondition<u64> = GroupingCondition {
    title: "same size",
    predicate: |path| -> u64 { path.metadata().unwrap().len() },
};

#[allow(clippy::unused_io_amount)]
pub const READ_FIRST_BYTES: GroupingCondition<[u8; SIZE]> = GroupingCondition {
    title: "read first bytes",
    predicate: |path| -> [u8; SIZE] {
        let mut fp = File::open(path).unwrap();
        let mut buf: [u8; SIZE] = [0; SIZE];

        fp.read(&mut buf).unwrap();

        buf
    },
};

#[allow(clippy::unused_io_amount)]
pub const READ_LAST_BYTES: GroupingCondition<[u8; SIZE]> = GroupingCondition {
    title: "read last bytes",
    predicate: |path| -> [u8; SIZE] {
        let size = min(path.metadata().unwrap().len() as i64, SIZE as i64);

        let mut fp = File::open(path).unwrap();
        let mut buf: [u8; SIZE] = [0; SIZE];
        fp.seek(SeekFrom::End(-size))
            .unwrap_or_else(|_| panic!("path: {:?}", path.to_str()));
        fp.read(&mut buf).unwrap();
        buf
    },
};

pub const SHA1_STRING: GroupingCondition<String> = GroupingCondition {
    title: "sha1",
    predicate: |path| {
        let mut hasher = Sha1::new();
        let bytes = read(path).unwrap();
        hasher.input(bytes.as_slice());
        hasher.result_str()
    },
};

#[cfg(test)]
mod test {
    use crate::grouping_condition::{
        FILE_SIZE, READ_FIRST_BYTES, READ_LAST_BYTES, SHA1_STRING, SIZE,
    };
    use std::fs::File;
    use std::io::Write;
    use std::path::PathBuf;
    use tempdir::TempDir;

    #[test]
    fn test_file_size() {
        let tmp = TempDir::new("test").unwrap();
        let (_buf, path) = make_random_file(&tmp);
        assert_eq!(SIZE, (FILE_SIZE.predicate)(&path) as usize);
    }

    #[test]
    fn test_read_first_bytes() {
        let tmp = TempDir::new("test").unwrap();
        let (buf, path) = make_random_file(&tmp);
        assert_eq!(buf, (READ_FIRST_BYTES.predicate)(&path));
    }

    #[test]
    fn test_read_last_bytes() {
        let tmp = TempDir::new("test").unwrap();
        let (buf, path) = make_random_file(&tmp);
        assert_eq!(buf, (READ_LAST_BYTES.predicate)(&path));
    }

    #[test]
    fn test_read_last_bytes_for_short_file() {
        let tmp = TempDir::new("test").unwrap();
        let (mut buf, path) = make_random_file_with_size(&tmp, 1);
        buf.resize(SIZE, 0);
        assert_eq!(buf, (READ_LAST_BYTES.predicate)(&path));
    }

    #[test]
    fn test_sha1_string() {
        let tmp = TempDir::new("test").unwrap();
        let original = tmp.path().to_path_buf().join("not_empty");
        {
            let mut fp = File::create(tmp.path().join(&original)).unwrap();
            fp.write_all("The quick brown fox jumps over the lazy dog".as_bytes())
                .unwrap();
        }
        assert_eq!(
            "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12",
            (SHA1_STRING.predicate)(&original)
        );
    }

    fn make_random_file(tmp: &TempDir) -> (Vec<u8>, PathBuf) {
        make_random_file_with_size(tmp, SIZE)
    }

    fn make_random_file_with_size(tmp: &TempDir, size: usize) -> (Vec<u8>, PathBuf) {
        let buf: Vec<u8> = (0..size).map(|_| rand::random::<u8>()).collect();
        let path = tmp.path().to_path_buf().join("not_empty");
        {
            let mut fp = File::create(tmp.path().join(&path)).unwrap();
            fp.write_all(&buf).unwrap();
        }
        (buf, path)
    }
    // pub struct Func {
    //     pub predicate: Box<dyn (Fn() -> bool )>,
    // }
    // use crate::grouping_condition::{Func};
    // use std::ops::BitXor;
    //
    // #[test]
    // fn test() {
    //     let flag = true;
    //     let f2 = Func {
    //         predicate: Box::new(move || { flag.bitxor(true) })
    //     };
    //
    //     assert!((f2.predicate)());
    // }
}
