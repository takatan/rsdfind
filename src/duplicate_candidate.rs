use crate::file_group::FileGroup;
use crate::filter_condition::FilterCondition;
use crate::grouping_condition::GroupingCondition;
use crate::history::History;
use std::fmt::Debug;

#[derive(Debug)]
pub struct DuplicateCandidate {
    pub groups: Vec<FileGroup>,
    pub history: Vec<History>,
}

impl DuplicateCandidate {
    pub fn new(base_dir: &[String]) -> Self {
        Self {
            groups: vec![base_dir.iter().map(|dir| FileGroup::new(dir)).sum()],
            history: Vec::new(),
        }
    }

    pub fn filter_by(&self, condition: &FilterCondition) -> Self {
        let groups = self.groups.iter().map(|g| g.filter_by(condition)).collect();
        let mut history = self.history.clone();
        history.push(History {
            action: format!("filter by: {}", condition.title),
            before: self.len(),
        });
        Self { groups, history }
    }

    pub fn group_by<T: Eq + Ord + Debug>(&self, condition: &GroupingCondition<T>) -> Self {
        let groups = self
            .groups
            .iter()
            .flat_map(|g| g.group_by(condition))
            .collect::<Vec<_>>();
        let mut history = self.history.clone();
        history.push(History {
            action: format!("group by: {}", condition.title),
            before: self.len(),
        });
        Self { groups, history }
    }

    pub fn filter_inode_duplicate(&self) -> Self {
        let mut history = self.history.clone();
        history.push(History {
            action: "remove inode duplicate".to_string(),
            before: self.len(),
        });

        Self {
            groups: self
                .groups
                .iter()
                .map(|g| g.filter_inode_duplicate())
                .collect(),
            history,
        }
    }

    pub fn len(&self) -> usize {
        self.groups.iter().map(|f| f.files.len()).sum()
    }

    pub fn file_size(&self) -> u64 {
        self.groups.iter().map(|g| g.file_size()).sum()
    }

    pub fn report(&self) {
        self.simple_report();
        for i in self.groups.iter() {
            print!("{}", i);
        }
    }

    pub fn simple_report(&self) {
        if let Some(h) = self.history.last() {
            println!(
                "=== {}, {} -> {}, {} bytes",
                h.action,
                h.before,
                self.len(),
                self.file_size()
            );
        };
    }
}
