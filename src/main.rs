// #![deny(warnings)]

/*!
Duplicate file find tool.

Usage: rsdfind [options] FILE or DIR ...

ref: rdfind https://github.com/pauldreik/rdfind
 */

use std::env::args;

use crate::duplicate_candidate::DuplicateCandidate;
use crate::filter_condition::SIZE_NOT_ZERO;
use crate::grouping_condition::{FILE_SIZE, READ_FIRST_BYTES, READ_LAST_BYTES, SHA1_STRING};
use getopts::Options;
use tap::Tap;

mod duplicate_candidate;
mod file_group;
mod filter_condition;
mod grouping_condition;
mod history;

const VERSION: &str = env!("CARGO_PKG_VERSION");

// ow scanning ".", found 47 files.
// Now scanning "/data/file0", found 5886 files.
// Now scanning "/data/file1", found 4327 files.
// Now scanning "/data/file2", found 2450 files.
// Now scanning "/data/file3", found 1146 files.
// Now scanning "/data/file7", found 1167 files.
// Now scanning "/data/file8", found 220 files.
// Now have 17264 files in total.
// Removed 0 files due to non unique device and inode.
// Total size is 29503437217175 bytes or 27 TiB
// Removed 16639 files due to unique sizes from list. 625 files left.
// Now eliminating candidates based on first bytes:removed 272 files from list. 353 files left.

fn print_usage(opts: Options) {
    println!("rsdfind: {}", VERSION);
    println!();
    print!("{}", opts.usage("Usage: rsdfind [options] FILE or DIR ..."))
}

fn main() {
    // let args: Vec<String> = args().skip(1).collect();
    let args: Vec<String> = args().collect();
    let mut opts = Options::new();
    opts.optflag("h", "verbose", "print this help menu");
    opts.optflag("V", "help", "print verbose log");
    let matches = opts.parse(&args[1..]).unwrap();
    if matches.opt_present("h") || matches.free.is_empty() {
        print_usage(opts);
        return;
    };
    let sample_report = if matches.opt_present("V") {
        DuplicateCandidate::simple_report
    } else {
        |_: &DuplicateCandidate| {}
    };

    let report = if matches.opt_present("V") {
        DuplicateCandidate::report
    } else {
        |_: &DuplicateCandidate| {}
    };

    let candidate = DuplicateCandidate::new(&matches.free);

    println!(
        "Total files: {}, size = {} bytes",
        candidate.len(),
        candidate.file_size()
    );

    candidate
        .filter_by(&SIZE_NOT_ZERO)
        .tap(sample_report)
        .filter_inode_duplicate()
        .tap(sample_report)
        .group_by(&FILE_SIZE)
        .tap(sample_report)
        .group_by(&READ_FIRST_BYTES)
        .tap(sample_report)
        .tap(report)
        .group_by(&READ_LAST_BYTES)
        .tap(sample_report)
        .tap(report)
        .group_by(&SHA1_STRING)
        .tap(DuplicateCandidate::simple_report)
        .tap(DuplicateCandidate::report);
}
